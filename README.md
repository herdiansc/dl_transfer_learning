## MobileNets Transfer Learning

TODO:
- Populate and create confusion matrix plot
- Record timing for every process:
    - Training
    - Predicting
    - Compiling
- Populate data:
    - Model size
    - Parameters size
    - Aws instance details
- Increate dataset to be 10 class
- Install image-match and elasticsearch to check image similarity