import shutil
import os, sys
import re
# import cv2

partition_file = '/home/jupyter/dl_transfer_learning/list_eval_partition.txt'

splitter = re.compile("\s+")
source_path = '/home/jupyter/img/'
destination_path = '/home/jupyter/deepfashion-dataset-splitted/'

if not os.path.exists(destination_path):
    os.mkdir(destination_path)

if not os.path.exists(destination_path + 'train/'):
    os.mkdir(destination_path + 'train/')

if not os.path.exists(destination_path + 'val/'):
    os.mkdir(destination_path + 'val/')

if not os.path.exists(destination_path + 'test/'):
    os.mkdir(destination_path + 'test/')

def process_folders():
    # Read the relevant annotation file and preprocess it
    # Assumed that the annotation files are under '<project folder>/data/anno' path
    with open(partition_file, 'r') as eval_partition_file:
        list_eval_partition = [line.rstrip('\n') for line in eval_partition_file][2:]
        list_eval_partition = [splitter.split(line) for line in list_eval_partition]
        list_all = [(v[0][4:], v[0].split('/')[1].split('_')[-1], v[1]) for v in list_eval_partition]

    # Put each image into the relevant folder in train/test/validation folder
    for element in list_all:
        dest_folder = destination_path + element[2] + '/' + element[1]
        if not os.path.exists(dest_folder):
            os.mkdir(dest_folder)
        source = source_path + element[0]
        filenames = element[0].split('/')
        dest = dest_folder + '/' + filenames[0] + '_' + filenames[1]
        print('Copying ' + source + ' TO ' + dest)
#         sys.exit()
        shutil.copy(source, dest)

process_folders()
