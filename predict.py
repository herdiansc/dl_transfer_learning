import sys
import argparse
import numpy as np
from PIL import Image, ImageOps
import requests
from io import BytesIO
import matplotlib.pyplot as plt

from keras.preprocessing import image
from keras.models import load_model, model_from_json
from keras.applications.inception_v3 import preprocess_input

from keras.applications.mobilenet import relu6, DepthwiseConv2D

from keras.utils.generic_utils import CustomObjectScope

target_size = (224, 224) #fixed size for InceptionV3 architecture


def predict(model, img, target_size):
  """Run model prediction on image
  Args:
    model: keras model
    img: PIL format image
    target_size: (w,h) tuple
  Returns:
    list of predicted labels and their probabilities
  """
  if img.size != target_size:
    img = img.resize(target_size)

  plt.imshow(img)
  plt.axis('off')

  plt.figure()

  x = image.img_to_array(img)
  x = np.expand_dims(x, axis=0)
  # x = np.reshape(x, (28, 28, 1))
  x = preprocess_input(x)
  preds = model.predict(x)

  # proba = model.predict_proba(x, batch_size=1)
  # print(proba)
  
  # classes = model.predict_classes(x, batch_size=1)
  # print(classes)
  # y_proba = model.predict(x)
  # return np_utils.probas_to_classes(y_proba)
  
  return preds[0]


def plot_preds(preds):
  """Displays image and the top-n predicted probabilities in a bar graph
  Args:
    image: PIL image
    preds: list of predicted labels and their probabilities
  """
  labels = ("celana panjang","celana pendek","kemeja lengan panjang","kemeja lengan pendek")
  plt.barh([0, 1, 2, 3], preds, alpha=0.5)
  plt.yticks([0, 1, 2, 3], labels)
  plt.xlabel('Probability')
  plt.xlim(0,1.01)
  plt.tight_layout()
  plt.show()


if __name__=="__main__":
  a = argparse.ArgumentParser()
  a.add_argument("--image", help="path to image")
  a.add_argument("--image_url", help="url to image")
  a.add_argument("--model")

  args = a.parse_args()

  if args.image is None and args.image_url is None:
    a.print_help()
    sys.exit(1)

  with CustomObjectScope({'relu6': relu6,'DepthwiseConv2D': DepthwiseConv2D}):
    model = load_model(args.model)

  model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

  if args.image is not None:
    img = Image.open(args.image)
    preds = predict(model, img, target_size)
    plot_preds(preds)

  if args.image_url is not None:
    response = requests.get(args.image_url)
    
    img = Image.open(BytesIO(response.content))
    preds = predict(model, img, target_size)
    plot_preds(preds)

