import shutil
import os, sys, glob
import re
# import cv2

splitter = re.compile("\s+")
source_path = '/home/jupyter/img/'
destination_path = '/home/jupyter/deepfashion-dataset-splitted/'

total = {'train': 0, 'val': 0, 'test': 0}
splits = glob.glob(destination_path + '*')
for split in splits:
    categories = glob.glob(split + '/*')
    for category in categories:
        files = glob.glob(category + '/*')
        total[os.path.basename(split)] = total[os.path.basename(split)] + len(files)
#         print(category, len(files))
        
print(total)